import React, { Component } from 'react';
import './App.css';
import Modal from "./component/UI/Modal/Modal";
import Alerts from "./component/UI/Alerts/Alerts"

class App extends Component {

    state = {
        showModal: false,
        showAlert: false,
        type: '',
    };

    showModal = () => {
        this.setState({showModal: true});
    };

    showAlert = (type) => {
        this.setState({showAlert: true, type: type});
    };

    closed = () => {
        this.setState({showModal: false});
    };

    continued = () => {
        alert('You have chosen to continue!')
    };

    dismissAlert = () => {
        this.setState({showAlert: false})
    };

    configuration = [
        {type: 'primary', label: 'Continue', clicked: this.continued},
        {type: 'danger', label: 'Close', clicked: this.closed}
    ];

  render() {
    return (
      <div className="App">
          <button
              className="show-modal"
            onClick={this.showModal}
          >
              Show modal
          </button>
        <Modal
            show={this.state.showModal}
            title="Some kinda modal title"
        >
            <p className="modal-content">This is modal content</p>

            <button
                className={`close-modal btn-${this.configuration[1].type}`}
                onClick={this.configuration[1].clicked}
            >
                {this.configuration[1].label}
            </button>

            <button
                className={`close-modal btn-${this.configuration[0].type}`}
                onClick={this.configuration[0].clicked}
            >
                {this.configuration[0].label}
            </button>

        </Modal>
        <div className="alerts-block">
            <button className='btn btn-primary' onClick={() => this.showAlert('primary')}>primary alert</button>
            <button className='btn btn-danger' onClick={() => this.showAlert('danger')}>danger alert</button>
            <button className='btn btn-success' onClick={() => this.showAlert('success')}>success alert</button>
            <button className='btn btn-warning' onClick={() => this.showAlert('warning')}>warning alert</button>
        </div>

          <Alerts
              dismiss={this.dismissAlert}
              show={this.state.showAlert}
              type={this.state.type}
          />

      </div>
    );
  }
}

export default App;
