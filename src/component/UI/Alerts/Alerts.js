import React from 'react';
import "./Alerts.css"


const Alerts = (props) => {
    return (
        <div
            className={`alert alert-${props.type}`} role="alert"
            style={{
                display: props.show ? 'block' : 'none'
            }}
        >
            A simple {props.type} alert—check it out!
            {props.dismiss ?
            <button className="close-alert" onClick={props.dismiss}>X</button> : null}
        </div>
    );
};

export default Alerts;